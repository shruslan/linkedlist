﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyLinkedList2;



namespace MyLinkedListTests
{
	[TestClass]
	public class NewListTests
	{
		[TestMethod]
		public void NewListFromEmpty()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] { });

			MyLinkedList newList = linkedList.NewList();

			Assert.AreEqual(newList.Count, 0);
		}

		[TestMethod]
		public void ListWithEvenAmountOfElements()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] { 1,4,5,6});

			MyLinkedList newList = linkedList.NewList();

			Assert.AreEqual(newList.Count, 2);
		}

		[TestMethod]
		public void ListWithUnevenAmountOfElements()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] { 1, 4, 5, 6 ,7});

			MyLinkedList newList = linkedList.NewList();

			Assert.AreEqual(newList.Count, 3);
		}
	}
}
