﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyLinkedList2;

namespace MyLinkedListTests
{
	[TestClass]
	public class DivideTests
	{
		[TestMethod]
		public void DivideEmptyList()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] { });

			MyLinkedList[] lists = linkedList.Divide();

			Assert.AreEqual(lists[0].Count, 0);
			Assert.AreEqual(lists[1].Count, 0);
		}

		[TestMethod]
		public void DivideNormalList()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] { 1, 4, 5, 5, 5, 8, 9, 3 });

			MyLinkedList[] lists = linkedList.Divide();

			Assert.AreEqual(lists[0].Count, 2);
			Assert.AreEqual(lists[1].Count, 6);
		}

		[TestMethod]
		public void DivideOnlyToOne()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] { 3, 6, 9 });

			MyLinkedList[] lists = linkedList.Divide();

			Assert.AreEqual(lists[0].Count, 3);
			Assert.AreEqual(lists[1].Count, 0);
		}
	}
}
