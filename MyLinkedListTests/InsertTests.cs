﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyLinkedList2;

namespace MyLinkedListTests
{
	[TestClass]
	public class InsertTests
	{
		[TestMethod]
		public void InsertInEmptyList()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] { });

			linkedList.Insert(2);

			Assert.AreEqual(1, linkedList.Count);
		}

		[TestMethod]
		public void InsertInList()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] { 1, 3 });

			linkedList.Insert(2);

			Assert.AreEqual(3, linkedList.Count);
		}
	}
}
