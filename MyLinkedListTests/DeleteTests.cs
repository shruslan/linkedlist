﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyLinkedList2;


namespace MyLinkedListTests
{
	[TestClass]
	public class DeleteTests
	{
		[TestMethod]
		public void DeleteFromEmptyList()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] { });

			linkedList.Delete(1);

			Assert.AreEqual(0, linkedList.Count);
		}

		[TestMethod]
		public void NormalDelete()
		{
			MyLinkedList linkedlist = new MyLinkedList(new int[] { 1, 2, 3, 5 });

			linkedlist.Delete(2);

			Assert.AreEqual(3, linkedlist.Count);
		}

		[TestMethod]
		public void DeletionOfNonExistantNumber()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] { 1, 2, 3, 5 });

			linkedList.Delete(4);

			Assert.AreEqual(4, linkedList.Count);
		}
	}
}
