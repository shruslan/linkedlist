﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyLinkedList2;



namespace MyLinkedListTests
{
	[TestClass]
	public class MaxNumTests
	{
		[TestMethod]
		public void MaxNumInList()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] { 1,2,5,5,5,8,8});

			var max = linkedList.MaxNum();

			Assert.AreEqual(3, max);
		}

		[TestMethod]
		public void MaxInEmptyList()
		{
			MyLinkedList linkedList = new MyLinkedList(new int[] {});

			var max = linkedList.MaxNum();

			Assert.AreEqual(0, max);
		}
	}
}
