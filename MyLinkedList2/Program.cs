﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLinkedList2
{
	class Program
	{
		static void Main(string[] args)
		{
			MyLinkedList myLinkedList = new MyLinkedList(new int[] { 1, 4, 5, 5, 5, 8, 9, 3 });
			MyLinkedList myLinkedList2 = new MyLinkedList(new int[] { 6, 8, 10, 12 });

			Console.Write("First List: "); myLinkedList.Print(); Console.Write(". Count is " + myLinkedList.Count);
			Console.WriteLine();
			Console.Write("Second List: "); myLinkedList2.Print(); Console.Write(". Count is " + myLinkedList2.Count);
			Console.WriteLine();
			Console.WriteLine();

			myLinkedList.Delete(8);
			Console.Write("First List after deletion of 8: "); myLinkedList.Print(); Console.Write(". Count is " + myLinkedList.Count);
			Console.WriteLine();

			myLinkedList.Insert(5);
			Console.Write("First List after insertion of 5: "); myLinkedList.Print(); Console.Write(". Count is " + myLinkedList.Count);
			Console.WriteLine();

			Console.WriteLine("Max amount of same number is " + myLinkedList.MaxNum());
			Console.WriteLine();


			MyLinkedList newList = myLinkedList2.NewList();
			Console.Write("New List 1: "); newList.Print(); Console.Write(". Count is " + newList.Count);
			Console.WriteLine();

			MyLinkedList newList2 = myLinkedList.NewList();
			Console.Write("New List 2: "); newList2.Print(); Console.Write(". Count is " + newList2.Count);
			Console.WriteLine();
			Console.WriteLine();


			MyLinkedList[] lists = myLinkedList.Divide();
			Console.Write("First after divide: "); lists[0].Print(); Console.Write(". Count is " + lists[0].Count);
			Console.WriteLine();
			Console.Write("Second after divide: "); lists[1].Print(); Console.Write(". Count is " + lists[1].Count);
			Console.WriteLine();
			Console.WriteLine();

			MyLinkedList mergeList = lists[0].Merge(newList);
			Console.Write("Merge 'first after divide' with 'new list 1': "); mergeList.Print(); Console.Write(". Count is " + mergeList.Count);
			Console.WriteLine();

			Console.ReadKey();
			
		}
	}
}
