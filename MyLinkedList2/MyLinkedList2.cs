﻿using System;

namespace MyLinkedList2
{
	public class MyLinkedList
	{
		public class Node
		{
			public int Data { get; set; }
			public Node Next { get; set; }
			public Node Prev { get; set; }

			public Node(int value)
			{
				Data = value;
			}
		}

		public Node head { get; private set; }
		public Node tail { get; private set; }
		public int Count { get; private set; }

		public MyLinkedList(int[] arr)
		{
			foreach (var value in arr)
			{
				Insert(value);
			}
		}

		private MyLinkedList() { }

		public void Delete(int value)
		{
			if (head == null)
				return;
			Node current = head;

			while (current.Data != value && current.Next != null)
				current = current.Next;
			if (current.Data == value)
			{
				current.Prev.Next = current.Next;
				current.Next.Prev = current.Prev;
				Count--;
			}
		}
		
		public void Insert(int value)
		{
			Node node = new Node(value);
			if (head == null)
			{
				head = node;
				tail = node;
			}
			if (node.Data <= head.Data)
			{
				AddFirst(node);
			}
			else if (node.Data >= tail.Data)
			{
				AddLast(node);
			}
			else
			{
				Node temp = head;
				while (node.Data > temp.Data)
				{
					temp = temp.Next;
				}
				AddBetweenTwoNodes(node, temp);
			}
			Count++;
		}

		public int MaxNum()
		{
			if (head == null)
				return 0;
			Node temp = head;
			int value = temp.Data;
			int tempMax = 1;
			int max = 1;

			while (temp.Next != null)
			{
				temp = temp.Next;
				if (temp.Data == value)
					tempMax++;
				else
				{
					tempMax = 1;
					value = temp.Data;
				}
				if (tempMax > max) max = tempMax;
			}
			return max;
		}

		private void AddFirst(Node node)
		{
			Node temp = head;
			head = node;
			head.Next = temp;
			head.Next.Prev = head;
		}

		private void AddLast(Node node)
		{
			Node temp = tail;
			tail = node;
			tail.Prev = temp;
			tail.Prev.Next = tail;
		}

		private void AddBetweenTwoNodes(Node node, Node nextNode)
		{
			Node temp = nextNode.Prev;
			nextNode.Prev = node;
			nextNode.Prev.Prev = temp;
			nextNode.Prev.Prev.Next = nextNode.Prev;
			nextNode.Prev.Next = nextNode;
		}

		public void Print()
		{
			if (head != null)
			{
				Node temp = head;
				Console.Write(temp.Data + " ");

				while(temp.Next != null)
				{
					temp = temp.Next;
					Console.Write(temp.Data + " ");
				}
			}
		}

		public MyLinkedList Merge(MyLinkedList linkedList)
		{
			MyLinkedList newList = new MyLinkedList();
			Node first = head;

			newList.Insert(first.Data);
			while (first.Next != null)
			{
				first = first.Next;
				newList.Insert(first.Data);
			}

			first = linkedList.head;
			newList.Insert(first.Data);
			while(first.Next != null)
			{
				first = first.Next;
				newList.Insert(first.Data);
			}

			return newList;
		}
		
		public MyLinkedList NewList()
		{
			Node first = head;
			Node last = tail;
			MyLinkedList newList = new MyLinkedList();

			for (int i =0; i < Count/2 + Count % 2; i++)
			{
				int temp = first.Data * last.Data;
				first = first.Next;
				last = last.Prev;
				newList.Insert(temp);
			}
			return newList;
		}
		
		public MyLinkedList[] Divide()
		{
			MyLinkedList firstList = new MyLinkedList();
			MyLinkedList secondList = new MyLinkedList();
			MyLinkedList[] lists = new MyLinkedList[2];
			if (Count > 0){
				Node temp = head;

				if (temp.Data % 3 == 0)
					firstList.Insert(temp.Data);
				else secondList.Insert(temp.Data);

				while (temp.Next != null)
				{
					temp = temp.Next;
					if (temp.Data % 3 == 0)
						firstList.Insert(temp.Data);
					else secondList.Insert(temp.Data);
				}
			}

			lists[0] = firstList;
			lists[1] = secondList;
			return lists;
		}
		
	}
}
